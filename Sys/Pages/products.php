<?php

$page->title='Продукты';
function createContent(){
    ?>
<table  border="1">
    <tr>
        <td style="vertical-align: top">
            <ul id="menu" > 
            <li class="color"><p id ="cat_1"><a href="about">О компании</a></p></li>
            <li class="color"><p id ="cat_2"><a href="products">Продукты</a></p></li>
            <li class="color"><p id ="cat_3"><a href="services">Услуги</a></p></li>
            <li class="color"><p id ="cat_4"><a href="helpdesk">Техподдержка</a></p></li>
            <li class="color"><p id ="cat_5"><a href="news">Новости</a></p></li>
            <li class="color"><p id ="cat_6"><a href="contacts">Контакты</a></p></li>

            </ul>
        </td>
        <td id="td_h">
            <div id="text_box">
<h2>Продукты</h2>
<h>
    <h4>АБС SCROOGE</h4>
Комплексная автоматизированная банковская система, обеспечивающая интегрированное управление 
ресурсами банка и его эффективную деятельность в целом.
Позволяет оптимизировать работу всех бизнес подразделений банков.</h>
<br>
<h>
    <h4>Фронт-офисный комплекс WebBank</h4>
Программный комплекс WebBank предоставляет банку возможность организовать работу сотрудников, 
обслуживающих клиентов банка через единое приложение с удобным, 
легким веб-интерфейсом и расширенными возможностями и контролями на каждом из этапов работы. </h>
<br>
<h>
    <h4>Комплекс кассира Cash&Billing</h4>
Данный программный комплекс, объединяя в себе две системы "Cash&Billing" и "MoneyTransfer",
представляет собой универсальное рабочее место кассира банка.</h>
<br>
<h>
    <h4>LS-Crypt</h4>
Система криптографической защиты информации LS-Crypt является библиотекой криптопреобразований, 
основанных на математическом методе эллиптических кривых и служит для шифрования и дешифрования информации,
вычисления имитовставки, вычисления функции хеширования, формирования и проверки 
электронной цифровой подписи.</h>
<br>
<h>
    <h4>Клиент-банк Tiny</h4>
Система удаленного обслуживания клиентов Банка.</h>
<br>
<h>
    <h4>Интернет-банкинг iTiny</h4>
Программный комплекс дистанционного обслуживания клиентов банка (юридических и физических лиц),
предоставляющий доступ к финансовой информации клиента с возможностью выполнения определенного 
перечня операций и функций.</h>
<br>
            </div>
</td>
</tr>
</table>
    <?php
}