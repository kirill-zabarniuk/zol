<?php
class DriverDB{
    private $dbhost;
    private $dblogin;
    private $dbpassw;
    private $dbname;
    private $connection;
   


    public function __construct() {
        require 'Res/Configs/configdb.php';
        $this->dbhost = dbhost;
        $this->dblogin = dblogin;
        $this->dbpassw = dbpassw;
        $this->dbname = dbname;       
    }
    
    private function dbconnect(){
        $this->connection = mysql_connect($this->dbhost, $this->dblogin,$this->dbpassw,$this->dbname )
                or die('Ошибка соединения с БД:'.mysql_error());
        mysql_select_db($this->dbname)
                or die ('Ошибка выбора ДБ:'.mysql_error());
    }
    
    private function dbdisconnect(){
        mysql_close($this->connection)
                or die('Ошибка разъединения с сервером БД:'.mysql_error);
    }
    public function queryexecute1($query)
    {
        $this->dbconnect();
        mysql_query($query)
                or die ('Ошибка выполнения SQL-запроса:'.mysql_error());
        $this->dbdisconnect();
    }
    
     public function queryexecute2($query)
    {
        $this->dbconnect();
        $result=mysql_query($query)
                or die ('Ошибка выполнения SQL-запроса:'.mysql_error());
//        $N=  mysql_num_rows($result)
//                or die ('Ошибка определения количества записей:'.mysql_error());
//        for ($i=0; $i<$N; $i++)
//        {
//            $records[$i]=array($result['caption'], $result['text']);
//        }
        $this->dbdisconnect();
        return $result;
    }
}