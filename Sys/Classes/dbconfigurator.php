<?php

class DB_Configurator{
    public $dbhost;
    public $dblogin;
    public $dbpassw;
    public $dbname;
    
    public function __construct($p1,$p2, $p3, $p4) {
        $this->dbhost=$p1;
        $this->dblogin=$p2;
        $this->dbpassw=$p3;
        $this->dbname=$p4;
    }
}