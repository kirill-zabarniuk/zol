<?php

class PagesManager{
    public $id;
    public $user;
    public $title;
    public $style;
    public $script;
    public $content;
    public $template;
    
    public function __construct() {
        $this->setID();
        $this->setUser();
        $this->title='Название страницы';
        $this->style='Res/Styles/'.$this->id.'.css';
        $this->script='Res/Scripts/'.$this->id.'.js';
        $this->content='Sys/Pages/'.$this->id.'.php';
        $this->template='Res/Templates/base.php';

    }
    
    private function setID(){
       $id=  filter_input(INPUT_GET, 'id');
       if($id){
           $this->id=$id;
       } else {
           $this->id='about';
       }
    }
    private function setUser(){
        if (isset($_SESSION['user']))
            
       // $user=  filter_input(INPUT_SESSION, 'user');
       // if($user)
            {
            $this->user=$_SESSION['user'];
        } else {
            $user=  filter_input(INPUT_COOKIE, 'user');
            if($user){
                
                $this->user=$user;
            } else {
                $this->user='Guest';
            }
            }
        }
}
