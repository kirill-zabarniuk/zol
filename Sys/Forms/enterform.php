<table  border="1">
    <tr>
        <td style="vertical-align: top" width="22%">
            <ul id="menu" > 
            <li class="color"><p id ="cat_1"><a href="about">О компании</a></p></li>
            <li class="color"><p id ="cat_2"><a href="products">Продукты</a></p></li>
            <li class="color"><p id ="cat_3"><a href="services">Услуги</a></p></li>
            <li class="color"><p id ="cat_4"><a href="helpdesk">Техподдержка</a></p></li>
            <li class="color"><p id ="cat_5"><a href="news">Новости</a></p></li>
            <li class="color"><p id ="cat_6"><a href="contacts">Контакты</a></p></li>

            </ul>
        </td>
        <td style="vertical-align: top" width="78%">


<form id="enter_form" action="#" method="post">
    <table>
        <!--=========================================== -->
        <tr>
            <td class="t1"> Логин:</td>           
            <td class="t2">
                <input type="text" id="login" name="login" placeholder="Ваш логин" required>   
            </td>
        </tr>  
        
        <tr>
            <td class="t1"> </td>           
            <td class="t2">
                <span id="error1" class="error"></span>  
            </td>
        </tr> 
        <!--=========================================== -->
        <tr>
            <td class="t1"> Пароль:</td>           
            <td class="t2">
                <input type="password" id="pass1" name="pass1" placeholder="Длина от 4 до 14 символов [a-z][a-zo-9_]{3,11}" required>   
            </td>
        </tr>  
        
        <tr>
            <td class="t1"> </td>           
            <td class="t2">
                <span id="error2" class="error"></span>  
            </td>
        </tr> 
        <!--=========================================== -->
        
        
    </table>
    <p>
        <input type="checkbox" name="allow" value="yes">
        Оставаться в системе
    </p>
    <p>
        <input type="submit" name="submit" value="Send" class="b1">
        <input type="reset" name="reset" value="Clear" class="b1">
        
    </p>
</form>
</td>
    </tr>
    </table>
