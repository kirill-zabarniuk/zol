<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title><?=$page->title?></title>
        <link rel="stylesheet" href="Res/Styles/base.css">
        <link rel="stylesheet" href="Res/Styles/footer.css">
        <link rel="stylesheet" href="Res/Styles/header.css">
        <link rel="stylesheet" href="Res/Styles/enter.css">
        <link rel="stylesheet" href="Res/Styles/menu.css">
        <link rel="stylesheet" href="<?=$page->style?>">
        <script src="Res/Scripts/jquery-2.1.3.js"></script>
        <script src="Res/Scripts/jquery.hoverIntent.js"></script>
        <script src="Res/Scripts/gmaps.js"></script>
        <script src="Res/Scripts/jquery.easing.1.3.js"></script>
        <script src="<?=$page->script?>"></script>
    </head>
    <body>
       <script>

              //var choice_cat
              var height = 150;
              $(document).ready(function ()
              {
                  $("ul#menu li").mouseover(function ()
                  {
                      $(this).animate({ height: '150px' }, { queue: false, duration: 200 })
                  }).mouseout(function ()
                  {                    
                      $(this).animate({ height: '50px' }, { queue: false, duration: 200 })
                  });

              });


          </script> 
        <div id="main_box">
        <header>
            <?php include 'Res/Templates/header.php';?>
            
        </header>
<!--        <article>
            <?php include 'Res/Templates/nav.php';?>
        </article>-->
        <article>
            <div id="content_bar"> <?php createContent(); ?></div>
<!--            <div id="left_bar">//<?php include 'Res/Templates/leftbar.php';?></div>-->

            </article>
        <footer>
            <?php include 'Res/Templates/footer.php';?>
        </footer>
            </div>
    </body>
</html>
